from random import randint

name = input("Hi! what is your name?  ")

 

for guess_number in range(1, 6):
    month_number = randint(1, 12)
    month_day = randint(1, 30)
    year_number = randint(1924, 2004)

    print("Guess", guess_number, "were you born in ", 
        month_number, "/",  month_day,  "/",  year_number,  "? ")

    response = input("yes or no?  ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number ==5:
        print("I am out of guesses", name, ". Have a great day!")

    else:
        print("Darn! Let me try again")  
        